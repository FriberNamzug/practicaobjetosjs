/* OBJETO PERSONA */
function Persona(nombre,sexo,edad){
    let persona = {
    nombre: nombre,
    sexo:   sexo,
    edad:  edad
    }
    return  persona
}

//////////////////////////
/* Actividades gerente y empleado */
let actividadesGerente = {
    AdministrarDeptos: function(){
        return `${this.nombre} tiene que dar ordenes para mejorar ventas`
    },
    RealizarNomina: function() {
        return `${this.nombre} tiene que pagar a empleado (id)`        
    }
}

let actividadesEmpleado = {
    RealizarInventario: function(){
        return `${this.nombre} tiene que contar producto faltante del area ${this.posicion}`
    },
    HacerVenta: function() {
        return `${this.nombre} tiene que hacer venta del producto (nombre)`        
    }
}

///////////////////////
/* constructor empleado y gerente */

let constructorGerente ={
    DEPARTAMENTO(){
        return `${this.nombre} esta en el departamento ${this.departamento}`
    },
    PERSONAS(){
        return `${this.nombre} esta a cargo de ${this.numeroP} personas`
    }
}
let constructorEmpleado ={
    DEPARTAMENTO(){
        return `${this.nombre} esta en el departamento ${this.departamento}`
    },
    POSICION(){
        return `${this.nombre} se encuentra en la posicion ${this.posicion}`
    }
}


///////////////////////////////////
/*  FUNCION PARA GERENTES  */
function Gerente(nombre,sexo,edad,numeroP,departamento){
    let gerente = Persona(nombre,sexo,edad)
    gerente.departamento = departamento
    gerente.numeroP = numeroP
    Object.setPrototypeOf(constructorGerente, actividadesGerente)
    Object.setPrototypeOf(gerente, constructorGerente)
    return gerente
}
///////////////////////////////////
/*  FUNCION PARA EMPLEADOS  */
 function Empleado(nombre,sexo,edad,departamento,posicion){
    let empleado = Persona(nombre,sexo,edad)
    empleado.departamento = departamento
    empleado.posicion = posicion
    Object.setPrototypeOf(constructorEmpleado, actividadesEmpleado)
    Object.setPrototypeOf(empleado, constructorEmpleado)
    return empleado
} 



/* Este sirvio de ayuda para ir verificando mediante la consola y poder
    crear las funciones de abajo de forma mas facil
     */

//      let gerente = new Gerente('Adrian Gutierrez','hombre', 35 , 5, 'Electronica')
//      console.log(gerente)
//      console.log(gerente.DEPARTAMENTO())
//      console.log(gerente.PERSONAS())
//      console.log(gerente.AdministrarDeptos())
//      
//      
//      let empleado = new Empleado('Cecilia Valdes','Mujer', 40, 'Hogar', 'Blancos')
//      
//      console.log(empleado)
//      console.log(empleado.DEPARTAMENTO())
//      console.log(empleado.POSICION())
//      



//////////////////////////////////
/* INFORMACION DEL LOS GERENTES */
_Gerente = [
    {
    nombre: "Adrían Gutíerrez",
    sexo: "Hombre",
    edad: 35,
    personas: 5,
    departamento:"Electrónica",
},
{    
    nombre: "María García",
    sexo: "Mujer",
    edad: 40,
    personas: 3,
    departamento:"Hogar",
},
{    
    nombre: "Marcela Flores",
    sexo: "Mujer",
    edad: 42,
    personas: 2,
    departamento:"Blancos",
}
]


//////////////////////////////////
/* INFORMACION DEL LOS EMPLEADOS */

_Empleados = [
    {
    nombre: "Sergio Rodriguez",
    sexo: "Hombre",
    edad: 35,
    departamento:"Electronicos",
    posicion: "Videojuegos",
},
    {
    nombre: "Cesar Lopez",
    sexo: "Hombre",
    edad: 37,
    departamento:"Electronicos",
    posicion: "Sonido",
},
    {
    nombre: "Adriana Ortiz",
    sexo: "Mujer",
    edad: 30,
    departamento:"Belleza",
    posicion: "Cosmeticos",
},
    {
    nombre: "Maricela Cruz",
    sexo: "Mujer",
    edad: 38,
    departamento:"Belleza",
    posicion: "Perfumeria",
},
    {
    nombre: "Rodrigo Chavez",
    sexo: "Hombre",
    edad: 35,
    departamento:"Hogar",
    posicion: "Electrodomesticos",
},
    {
    nombre: "Cecilia Valdes",
    sexo: "Mujer",
    edad: 40,
    departamento:"Hogar",
    posicion: "Blancos",

}
]


//////////////////////////////////
/* Funciones para crear el DOM */


const pantallaGerente = () => {

    let resultado  = document.getElementById('tablaGerentes')
    let fragment = document.createDocumentFragment()

    for(let item of _Gerente) {
       let tr = document.createElement('tr')
       let comentario = new Gerente(item.nombre,item.sexo, item.edad , item.personas, item.departamento)
       console.log(comentario)

       let arraytd = [
                    item.nombre,
                    item.sexo, 
                    item.edad,
                    item.departamento,
                    comentario.AdministrarDeptos(),
                    comentario.PERSONAS()
                ]
    
       for(let i = 0; i < arraytd.length; ++i) {
          let td = document.createElement('td')
          td.appendChild(document.createTextNode(arraytd[i]))     
          tr.appendChild(td)
          fragment.appendChild(tr)
       }
    }
  
    resultado.appendChild(fragment)



}

const pantallaEmpleado = () => {

    let resultado  = document.getElementById('tablaEmpleados')
    let fragment = document.createDocumentFragment()
    
  
    for(let item of _Empleados) {
       let tr = document.createElement('tr')
       let comentario = new Empleado(item.nombre,item.sexo, item.edad, item.departamento,item.posicion)
       console.log(comentario)
       let arraytd = [
                        item.nombre,
                        item.sexo,
                        item.edad,
                        item.posicion,
                        item.departamento,
                        comentario.RealizarInventario()
                    ]
    
       for(let i = 0; i < arraytd.length; ++i) {
          let td = document.createElement('td')
          td.appendChild(document.createTextNode(arraytd[i]))     
          tr.appendChild(td)
          fragment.appendChild(tr)
       }
    }
  
    resultado.appendChild(fragment)



}



/* EJECUTAR FUNCIONES DE LA PANTALLA */

pantallaEmpleado()
pantallaGerente()